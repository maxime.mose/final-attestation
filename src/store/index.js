import { createStore } from 'vuex'

export default createStore({
  state: {
    Products: [],
    AddedItems: [],
    inputValues: {},
    isModalOpened: false,
    isGoToMainPage: false
  },
  getters: {
    GetProducts: state => state.Products,
    GetAddedItems: state => state.AddedItems,
    GetCount: state => state.AddedItems.length ? state.AddedItems.map(item => item.count).reduce((prev, next) => prev + next) : 0,
    GetSum: state => state.AddedItems.length
      ? state.AddedItems
        .map(item => item.count * state.Products.find(product => item.id === product.id).price)
        .reduce((prev, next) => prev + next)
      : 0,
    GetInputValues: state => {
      console.log('inputValues', state.inputValues)
      return state.inputValues
    },
    GetIsModalOpened: state => state.isModalOpened,
    GetIsGoToMainPage: state => state.isGoToMainPage
  },
  mutations: {
    SetAddItem: (state, id) => {
      const item = state.AddedItems.find(item => id === item.id)
      item ? item.count++ : state.AddedItems.push({ id: id, count: 1 })
    },
    SetRemoveItem: (state, id) => {
      const item = state.AddedItems.find(item => id === item.id)
      item.count > 1 ? item.count-- : state.AddedItems = state.AddedItems.filter(item => id !== item.id)
    },
    SetRemoveAllItems: (state) => {
      state.AddedItems = []
    },
    SetAddProducts: (state, products) => {
      state.Products.push(...products)
    },
    SetInputValue: (state, input) => {
      console.log('inputName', input.inputName)
      console.log('inputValue', input.inputValue)
      console.log('inputValues', state.inputValues)
      state.inputValues[input.inputName] = input.inputValue
    },
    SetIsModalOpened: (state, isModalOpened) => {
      console.log('isOrderModalOpened', state.isModalOpened)
      state.isModalOpened = isModalOpened
      console.log('isOrderModalOpened', state.isModalOpened)
    },
    SetIsGoToMainPage: (state, isGoToMainPage) => {
      console.log('isGoToMainPage', state.isGoToMainPage)
      state.isGoToMainPage = isGoToMainPage
      console.log('isGoToMainPage', state.isGoToMainPage)
    },
    SetClearProducts: (state) => {
      state.Products = []
    }
  }
})
