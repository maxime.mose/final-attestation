import { createRouter, createWebHistory } from 'vue-router'
import MainPage from '../views/MainPage'
import CartPage from '../views/CartPage'
import StartPage from '../views/StartPage'
import ProductPage from '../views/ProductPage'

const routes = [
  {
    path: '/',
    component: MainPage
  },
  {
    path: '/cart',
    component: CartPage
  },
  {
    path: '/start',
    component: StartPage
  },
  {
    path: '/card/:id',
    component: ProductPage
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
