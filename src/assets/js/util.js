export const prettifySum = sum => String(sum).split(/(?=(?:...)*$)/).join(' ')

export const getWordEnding = (count) => {
  if ([0, 5, 6, 7, 8, 9].includes(count % 10) || [11, 12, 13, 14].includes(count % 100)) {
    return 'ов'
  }
  if ([2, 3, 4].includes(count % 10)) {
    return 'a'
  }
  return ''
}
